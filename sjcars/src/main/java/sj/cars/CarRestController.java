package sj.cars;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;


@ApplicationScoped
@Path("cars")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CarRestController {

    private static Map<String, CarDTO> CARS = new ConcurrentHashMap<>();

    @Context
    UriInfo uriInfo;

    @Inject
    CarDAO carDAO;

    @Inject
    CarMapper mapper;

    @GET
    public  Response gets(){
        Stream<Car> cars = carDAO.findAll();
        if (cars == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(mapper.map(cars)).build();
    }

    @GET
    @Path("/{id}")
    public Response getCarById(@PathParam("id") String id) {
        Car car = carDAO.findById(id);
        if (car == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(mapper.map(car)).build();
    }

    @POST
    public Response createCar(CarDTO carDTO) throws Exception {
        Car car = mapper.create(carDTO);

        car = carDAO.create(car);

        return Response.created(uriInfo.getAbsolutePathBuilder().path(car.getId()).build())
                .entity(car)
                .build();
    }

    @PUT
    @Path("/{id}")
    public Response updateCar(@PathParam("id") String id, CarDTO carDTO) {
        Car car = carDAO.findById(id);
        if (car == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        car.setMark(carDTO.getMark());
        car.setModel(carDTO.getModel());
        car = carDAO.update(car);
        return Response.ok(mapper.map(car)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteCar(@PathParam("id") String id) throws Exception{

        carDAO.deleteById(id);
        return Response.ok().build();

    }

}
