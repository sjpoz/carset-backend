package sj.cars;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CarDTO {
    private String id;

    private String mark;

    private String model;
}
