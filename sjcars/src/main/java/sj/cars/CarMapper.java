package sj.cars;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sj.cars.Car;
import sj.cars.CarDTO;

import java.util.stream.Stream;

@Mapper
public interface CarMapper {

    CarDTO map(Car model);

    Stream<CarDTO> map(Stream<Car> model);

    @Mapping(target = "id", ignore = true)
    Car create(CarDTO dto);
}
