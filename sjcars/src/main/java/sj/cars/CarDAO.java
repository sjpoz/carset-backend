package sj.cars;

import org.tkit.quarkus.jpa.daos.AbstractDAO;
import sj.cars.Car;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;

@ApplicationScoped
public class CarDAO extends AbstractDAO<Car>{

    @Transactional(Transactional.TxType.REQUIRED)
    public Car deleteById(String id) throws Exception{
        Car car = findById(id);
        if (car == null) {
            throw new Exception();
        }
        delete(car);
        return car;
    }
}
