package sj.cars;

import lombok.Getter;
import lombok.Setter;
import org.tkit.quarkus.jpa.models.TraceableEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "T_CAR")
public class Car extends TraceableEntity{

    private String mark;

    private String model;
}
